# 遺伝子情報の検索プログラム
このレポジトリには、レポートに添付してあるファイルがおいてある。

## 前提条件
私はLinux(Ubuntu 18.04)環境で実行しているため他の環境では動くかわからない。

### Ubuntuでのパッケージ
```bash
$ apt install meson ninja-build clang
```

## 実行
必要なパッケージがインストールされていれば次の方法で実行できる
```bash
$ chmod +x ./run-regex.sh ./run-match.sh ./run-wildcard.sh
$ ./run-regex.sh
or ./run-match.sh
or ./run-wildcard.sh
# ビルド関連の出力
input pattern: AACC**T # <- ここにパターンを入力
Search pattern is: AACC**T
...
...
...
Found match at: 18020
Found match at: 19233
Found match at: 19630
Found match at: 19867
47 matches found
```

