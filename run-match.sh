#!/bin/bash

if [ ! -d build ]; then
    CC=clang CXX=clang++ meson build
fi

ninja -C build 

if [ $? == 0 ]; then
    if [ ! -f DNA.txt ]; then
        ./build/dna-generator > DNA.txt
    fi

    # main binary
    ./build/dna-match
fi
