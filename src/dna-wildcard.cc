#include <iostream>
#include <vector>

using namespace std;

typedef unsigned int uint;

bool getPattern();
bool importDNAString(string *);
vector<int> searchMatchingDNA(string &, string &);
bool recursivly_check_match(char *, char *, int);

string pattern;

int main() {
    if (!getPattern()) {
        cerr << "Invalid Pattern" << endl;
        return 1;
    };
    cout << "Search pattern is: " << pattern << endl;

    string DNA;
    if (!importDNAString(&DNA)) {
        cerr << "Could not open \"DNA.txt\".";
        return 2;
    }

    vector<int> match_positions = searchMatchingDNA(DNA, pattern);

    for (int i = 0; i < match_positions.size(); i++) {
        cout << "Found match at: " << match_positions[i] << endl;
    }

    switch (match_positions.size()) {
    case 0:
        cout << "No matches found" << endl;
        break;
    case 1:
        cout << "1 match found" << endl;
        break;
    default:
        cout << match_positions.size() << " matches found" << endl;
    }

    return 0;
}

vector<int> searchMatchingDNA(string &DNA, string &pattern) {
    vector<int> match_positions;
    for (int i = 0; i < (DNA.length() - pattern.length() + 1); i++) {
        bool is_matching_pattern;

        is_matching_pattern = recursivly_check_match(&DNA[i], &pattern[0], pattern.length());

        if (is_matching_pattern) {
            match_positions.push_back(i + 1);
        }
    }

    return match_positions;
}

bool recursivly_check_match(char *DNA_offseted, char *pattern_offseted,
                            int length) {
    if (length == 0) {
        return true;
    }

    if (*pattern_offseted == '*') {
        return recursivly_check_match(DNA_offseted, pattern_offseted + 1,
                                      length - 1) |
               recursivly_check_match(DNA_offseted + 1, pattern_offseted + 1,
                                      length - 1);
    }

    if (*DNA_offseted == *pattern_offseted) {
        return recursivly_check_match(DNA_offseted + 1, pattern_offseted + 1,
                                      length - 1);
    }

    return false;
}

bool importDNAString(string *dna_string) {
    FILE *fd = fopen("DNA.txt", "r");
    if (fd == nullptr)
        return false;

    char c;
    while ((c = fgetc(fd)) > -1) {
        *dna_string += c;
    }

    fclose(fd);

    return true;
}

bool getPattern() {
    cout << "input pattern: ";
    cin >> pattern;

    for (char &c : pattern) {
        c = toupper(c);
        switch (c) {
        case 'A':
        case 'T':
        case 'G':
        case 'C':
        case '*':
            break;
        default:
            return false;
        }
    }

    return true;
}
