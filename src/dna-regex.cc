#include <iostream>
#include <regex>
#include <string>
#include <vector>

using namespace std;

bool getPattern(string *);
bool importDNAString(string *);

int main() {
    string pattern, DNA;
    if (!getPattern(&pattern))
        return 1;
    if (!importDNAString(&DNA))
        return 1;

    cout << "search regex pattern: " << pattern << endl;

    regex search_regex(pattern);
    auto begin = sregex_iterator(DNA.begin(), DNA.end(), search_regex);
    auto end = sregex_iterator();

    int count = 0;
    for (auto i = begin; i != end; i++) {
        count++;
        cout << "Found match at: " << (*i).position() + 1 << endl;
    }
    cout << count << " matches found" << endl;

    return 0;
}

bool getPattern(string *pattern) {
    string input;
    cout << "input pattern: ";
    cout.flush();
    cin >> input;

    if (input == "")
        return false;

    (*pattern) = "(?=(";

    for (char c : input) {
        c = toupper(c);
        switch (c) {
        case 'A':
        case 'T':
        case 'G':
        case 'C':
            (*pattern) += c;
            break;
        case '*':
            (*pattern) += ".?";
            break;
        default:
            return false;
        }
    }

    (*pattern) += "))";

    return true;
}

bool importDNAString(string *dna_string) {
    FILE *fd = fopen("DNA.txt", "r");
    if (fd == nullptr)
        return false;

    char c;
    while ((c = fgetc(fd)) > -1) {
        *dna_string += c;
    }

    fclose(fd);

    return true;
}
