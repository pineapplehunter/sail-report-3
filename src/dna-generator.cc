#include<iostream>
#include<random>

using std::cout;

const char DNA[4] = {'A','T','G','C'};

int main(int argc, char const *argv[])
{
    for (int i = 0; i < 20000; i++){
        char random_dna = DNA[rand()%4];
        cout << random_dna;
    }

    cout.flush();

    return 0;
}
